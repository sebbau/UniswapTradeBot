import { ChainId, Token, Fetcher, Route, Trade, TokenAmount, TradeType, Percent, BigintIsh } from "@uniswap/sdk";
import { ethers, Wallet, BigNumber } from "ethers";
import { constants } from "./Constants"

export class Swap {
    private _tokenFrom: Token;
    private _tokenTo: Token;
    private _slippage: string;
    private _valueToSwap: string;
    private _valueToSwapUnit: number;

    constructor(tokenFrom: Token, tokenTo: Token, slippage: string, valueToSwap: string, valueToSwapUnit: number) {
        this._tokenFrom = tokenFrom;
        this._tokenTo = tokenTo;
        this._slippage = slippage;
        this._valueToSwap = valueToSwap;
        this._valueToSwapUnit = valueToSwapUnit;
    }

    async swap(): Promise<void> {
        const pairData = await Fetcher.fetchPairData(this._tokenFrom, this._tokenTo);
        const route = new Route([pairData], this._tokenFrom);
        const trade = new Trade(route, new TokenAmount(this._tokenFrom, this._valueToSwap), TradeType.EXACT_INPUT);
        const slippageTolerance = new Percent(this._slippage, "1000");
        const amountIn = ethers.utils.parseUnits(String(trade.inputAmount.raw), this._valueToSwapUnit);
        const amountOutMin = trade.minimumAmountOut(slippageTolerance).raw;
        const path = [this._tokenFrom.address, this._tokenTo.address];
        const to = constants.walletAddress;
        const deadline = Math.floor(Date.now() / 1000) + 60 * constants.deadline;
    
        const provider = ethers.getDefaultProvider(constants.networkName, { alchemy: constants.provider });
    
        const walletMnemonic = Wallet.fromMnemonic(constants.walletMnemonics);
    
        const account = walletMnemonic.connect(provider);
    
        const uniswap = new ethers.Contract(constants.contractAddress, [constants.contractABI], account);
    
        const tx = await uniswap.swapExactTokensForTokens(String(amountIn), String(amountOutMin), path, to, deadline, { gasPrice: constants.gasPrice, gasLimit: constants.gasLimit });
    
        console.log("Tx Hash : " + tx.hash);
        console.log("---------------------------------------------");
    };
}