import { ChainId, Token } from "@uniswap/sdk";

export const constants = {
    walletAddress: "XXXX",
    walletMnemonics: "XXXX",
    deadline: 20,
    networkName: "rinkeby",
    provider: "XXXX",
    contractAddress: "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D",
    contractABI: "function swapExactTokensForTokens(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external returns (uint[] memory amounts)",
    gasPrice: 2e9,
    gasLimit: 999999,
    chainId: ChainId.RINKEBY,
} as const;

export const wETHTokenConstants = new Token(ChainId.RINKEBY, "0xc778417e063141139fce010982780140aa0cd5ab", 18, "wETH", "Wrapped Ether");

export const usdcTokenConstants = new Token(ChainId.RINKEBY, "0x5eca482a51e739df5473a0c09cd5de813d163ed5", 6, "USDC", "USD Coin Mock");

export const uniswapTokenConstants = new Token(ChainId.RINKEBY, "0x1f9840a85d5af5bf1d1762f925bdaddc4201f984", 18, "UNI", "Uniswap");