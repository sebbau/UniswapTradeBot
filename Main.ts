import { constants, uniswapTokenConstants, usdcTokenConstants, wETHTokenConstants } from "./Constants";
import { Swap } from "./Swap";

class Main {
    async swap() {
        var step;
        for (step = 0; step < 50; step++){
            var swap = new Swap(usdcTokenConstants, uniswapTokenConstants, "5", "10", 4);
            await swap.swap();
        }
    }
}

var main = new Main();
main.swap();