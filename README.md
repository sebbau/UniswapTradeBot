# Uniswap Trade Bot

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
Uniswap Trade Bot is a console program to perform automatic operations (swaps) on a decentralized Uniswap exchange, version 2.

## Technologies
* Uniswap SDK version: 3.0.3
* Uniswap SDK v2 version: 3.0.0
* Ethers.js version: 5.4.6
* Typescript version: 4.4.2
* TS Node version: 10.2.1
* Common.js version: 1.1.1

## Setup

### Configure Data
Before running any command and install dependencies set your data in Constants.ts

Data to set:
* walletAddress - the address of the wallet you'll be using
* walletMnemonics - your wallet mnemonics (!!! DON'T SHOW THIS DATA TO ANYONE !!!)
* deadline - maximum transaction time in minutes (default 20 minutes)
* networkName - ethereum network (default 'rinkeby')
* provider - your personal provider to sending operation to Uniswap Smart Contract (e.g. Infura, Alchemy)
* contractAddress - Uniswap Smart Contract address i.e. Router to execute all operations on Uniswap exchange (default set)
* contractABI - Uniswap Smart Contract ABI (default set for 'swapExactTokensForTokens' operation, more on https://docs.uniswap.org/protocol/V2/reference/smart-contracts/router-02)
* gasPrice - transaction gas price in 'gwei' (default 2 gwei)
* gasLimit - gas limit (default 999999)
* chainId - ethereum network with value from ethers.js (needed for tokens configuration, default 'Rinkeby' network)

They are configured 3 tokens by default in Constants.ts (wETH, USDC and Uniswap). This is the best place to add more tokens configuration.

### Configure code to execute
By default in Main.ts in 'swap' method  is set 'for' loop to execute swap transactions in loop. Change code in this method to your personal requirements.

### Install dependencies
Install dependencies using npm:

```
$ npm install
```

### Run
To run program use commans:

```
npx ts-node Main.ts
```

During program execution, you'll see the has of each transaction in console e.x.:

Tx Hash : 0x6b3ce616d2a39ab68e3c03cfc3c60ef371b09d0798fd4fd3af23dcab570bc1fa
---------------------------------------------
Tx Hash : 0xcfc5745797e791c32b9d129c263d0999bbcc327b3cc8908be457a1b8758525ae
---------------------------------------------
Tx Hash : 0xc642c6d9351bb56c1d731e2414d5edf86d00a3f9d11fb2293190835cbd8945bb
---------------------------------------------